#include <stdio.h>
#include <conio.h>
#include <iup.h>

int btn_exit_cb( Ihandle *self )
{
  /* Exits the main loop */
  IupMessage("exit", "exit now");
  return -3;
}

int main(int argc, char **argv)
{
  Ihandle *dlg, *button;

  IupOpen(&argc, &argv);
  
  //label =  IupLabel("Hello world from IUP.");
  button = IupButton("OK", NULL);
  

  
  dlg = IupDialog(button);
  IupSetAttribute(dlg, "TITLE", "Hello World 5");

  /* Registers callbacks */
  IupSetCallback(button, "ACTION", "btn_exit_cb");

  IupShowXY(dlg, 0, 0);

  IupMainLoop();

  IupClose();
  return 0;
}