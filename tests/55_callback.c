#include <stdio.h>
#include <conio.h>

int testFunc(void)
{
	printf("testFunc call\n");
	return 12345678;
}

void main()
{
    void *pFunc = (void*) "testFunc";
	setCallback(pFunc, (char *)pFunc);
	int retValue = callCallback(pFunc);
	printf("retValue = %d\n", retValue);
}
