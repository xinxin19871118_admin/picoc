#ifndef POINTER_LIST_H_
#define POINTER_LIST_H_


typedef struct{
	void * key;
	void * value;
	void * next;
} PointerToPointerList;

PointerToPointerList * newPointerToPointerList(void);
void freePointerToPointerList(PointerToPointerList * head, void (*freeValue)(void*));
PointerToPointerList * addPointerToPointerList(PointerToPointerList * head, void * key, void * value);
PointerToPointerList * removePointerToPointerList(PointerToPointerList * head, void * key);
void * delPointerToPointerList(PointerToPointerList * head, void * key);
int delValuePointerToPointerList(PointerToPointerList * head, void * key, void (*freeValue)(void*));
void *getPointerToPointerList(PointerToPointerList * head, void * key);
void *setPointerToPointerList(PointerToPointerList * head, void * key, void *value);
void *setOrAddPointerToPointerList(PointerToPointerList * head, void * key, void *value);
void printPointerToPointerList(PointerToPointerList * head);
void testPointerToPointerList(void);



























#endif

