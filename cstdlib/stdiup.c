/* conio.h library for large systems - small embedded systems use clibrary.c instead */
#include "../src/interpreter.h"

#ifndef BUILTIN_MINI_STDLIB

#include "iup.h"
#include "iup_config.h"
#include "pointerList.h"



void StdiupOpen(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupOpen((int*)Param[0]->Val->Pointer,(char***)Param[1]->Val->Pointer);
}

void StdiupClose(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    IupClose();
}

void StdiupMainLoop(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupMainLoop();
}

void StdiupMessage(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    IupMessage((const char *)Param[0]->Val->Pointer, (const char *)Param[1]->Val->Pointer);
}

void StdiupGetDialog(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupGetDialog(Param[0]->Val->Pointer);
}

void StdiupDestroy(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    IupDestroy(Param[0]->Val->Pointer);
}

void StdiupGetDialogChild(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupGetDialogChild(Param[0]->Val->Pointer, Param[1]->Val->Pointer);
}

void StdiupShow(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupShow(Param[0]->Val->Pointer);
}

void StdiupShowXY(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupShowXY(Param[0]->Val->Pointer, Param[1]->Val->Integer, Param[2]->Val->Integer);
}

void StdiupPopup(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupPopup(Param[0]->Val->Pointer, Param[1]->Val->Integer, Param[2]->Val->Integer);
}


void StdiupHide(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupHide(Param[0]->Val->Pointer);
}

void StdiupSetAttribute(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    IupSetAttribute(Param[0]->Val->Pointer, Param[1]->Val->Pointer, Param[2]->Val->Pointer);
}

void StdiupGetAttribute(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupGetAttribute(Param[0]->Val->Pointer, Param[1]->Val->Pointer);
}

void StdiupSetGlobal(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    IupSetGlobal(Param[0]->Val->Pointer, Param[1]->Val->Pointer);
}

void StdiupGetGlobal(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupGetGlobal(Param[0]->Val->Pointer);
}



void StdiupDialog(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupDialog(Param[0]->Val->Pointer);
}

void StdiupButton(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupButton(Param[0]->Val->Pointer, Param[1]->Val->Pointer);
}


extern void PicocParse(const char *FileName, const char *Source, int SourceLen, int RunIt, int CleanupNow, int CleanupSource);
extern struct ValueType IntType;


static PointerToPointerList * _list_action = NULL;


void _setActionCallback(void *key, void *value)
{
	if(_list_action == NULL)
	{
		_list_action = newPointerToPointerList();
	}
	setOrAddPointerToPointerList(_list_action,key,value);
}

void *_getActionCallback(void *key)
{
	if(_list_action == NULL)
	{
		_list_action = newPointerToPointerList();
	}
	return getPointerToPointerList(_list_action,key);
}


int StdiupActionCallbackExitValue = 0;

int __actionCallback(Ihandle *ih)
{
	char buf[512];
	StdiupActionCallbackExitValue = IUP_DEFAULT;
	sprintf(buf, "__actionCallback_exit_value=%s((void*)0x%p);", (char*)_getActionCallback(ih), (void*)ih);

	if (!VariableDefined(TableStrRegister("__actionCallback_exit_value")))
	{
        VariableDefinePlatformVar(NULL, "__actionCallback_exit_value", &IntType, (union AnyValue *)&StdiupActionCallbackExitValue, TRUE);
	}
    
    PicocParse("startup", buf, strlen(buf), TRUE, TRUE, FALSE);

	return StdiupActionCallbackExitValue;
}


void StdiupGetCallback(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = (void*) _getActionCallback(Param[0]->Val->Pointer);
}

void StdiupSetCallback(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
	_setActionCallback(Param[0]->Val->Pointer, Param[2]->Val->Pointer);
    IupSetCallback(Param[0]->Val->Pointer, Param[1]->Val->Pointer, __actionCallback);
}

#define MAX_IUP_ARGS 16
Ihandle *StdiupBaseVbox(struct ParseState *Parser, void * ih, struct Value **Param, int NumArgs)
{
    struct Value *ThisArg = Param[0];
    int ArgCount = 0;
    void *ScanfArg[MAX_IUP_ARGS];
    
    if (NumArgs > MAX_IUP_ARGS)
        ProgramFail(Parser, "too many arguments to IupVbox() - %d max", MAX_IUP_ARGS);
    
    for (ArgCount = 0; ArgCount < NumArgs; ArgCount++)
    {
        ThisArg = (struct Value *)((char *)ThisArg + MEM_ALIGN(sizeof(struct Value) + TypeStackSizeValue(ThisArg)));
        
        if (ThisArg->Typ->Base == TypePointer) 
            ScanfArg[ArgCount] = ThisArg->Val->Pointer;
        
        else if (ThisArg->Typ->Base == TypeArray)
            ScanfArg[ArgCount] = &ThisArg->Val->ArrayMem[0];
        
        else
            ProgramFail(Parser, "non-pointer argument to IupVbox() - argument %d after format", ArgCount+1);
    }
	for (; ArgCount < MAX_IUP_ARGS; ArgCount++)
	{
		ScanfArg[ArgCount] = NULL;
	}
    
    return IupVbox((Ihandle*)ih, ScanfArg[0], ScanfArg[1], ScanfArg[2], ScanfArg[3], ScanfArg[4], ScanfArg[5], ScanfArg[6], ScanfArg[7], ScanfArg[8], ScanfArg[9],
		ScanfArg[10], ScanfArg[11], ScanfArg[12], ScanfArg[13], ScanfArg[14], ScanfArg[15]);
}


void StdiupVbox(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = StdiupBaseVbox(Parser, Param[0]->Val->Pointer, Param, NumArgs-1);
}

Ihandle *StdiupBaseHbox(struct ParseState *Parser, void * ih, struct Value **Param, int NumArgs)
{
    struct Value *ThisArg = Param[0];
    int ArgCount = 0;
    void *ScanfArg[MAX_IUP_ARGS];
    
    if (NumArgs > MAX_IUP_ARGS)
        ProgramFail(Parser, "too many arguments to IupHbox() - %d max", MAX_IUP_ARGS);
    
    for (ArgCount = 0; ArgCount < NumArgs; ArgCount++)
    {
        ThisArg = (struct Value *)((char *)ThisArg + MEM_ALIGN(sizeof(struct Value) + TypeStackSizeValue(ThisArg)));
        
        if (ThisArg->Typ->Base == TypePointer) 
            ScanfArg[ArgCount] = ThisArg->Val->Pointer;
        
        else if (ThisArg->Typ->Base == TypeArray)
            ScanfArg[ArgCount] = &ThisArg->Val->ArrayMem[0];
        
        else
            ProgramFail(Parser, "non-pointer argument to IupHbox() - argument %d after format", ArgCount+1);
    }
	for (; ArgCount < MAX_IUP_ARGS; ArgCount++)
	{
		ScanfArg[ArgCount] = NULL;
	}
    
    return IupHbox((Ihandle*)ih, ScanfArg[0], ScanfArg[1], ScanfArg[2], ScanfArg[3], ScanfArg[4], ScanfArg[5], ScanfArg[6], ScanfArg[7], ScanfArg[8], ScanfArg[9],
		ScanfArg[10], ScanfArg[11], ScanfArg[12], ScanfArg[13], ScanfArg[14], ScanfArg[15]);
}


void StdiupHbox(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = StdiupBaseHbox(Parser, Param[0]->Val->Pointer, Param, NumArgs-1);
}

Ihandle *StdiupBaseGridBox(struct ParseState *Parser, void * ih, struct Value **Param, int NumArgs)
{
    struct Value *ThisArg = Param[0];
    int ArgCount = 0;
    void *ScanfArg[MAX_IUP_ARGS];
    
    if (NumArgs > MAX_IUP_ARGS)
        ProgramFail(Parser, "too many arguments to IupGridBox() - %d max", MAX_IUP_ARGS);
    
    for (ArgCount = 0; ArgCount < NumArgs; ArgCount++)
    {
        ThisArg = (struct Value *)((char *)ThisArg + MEM_ALIGN(sizeof(struct Value) + TypeStackSizeValue(ThisArg)));
        
        if (ThisArg->Typ->Base == TypePointer) 
            ScanfArg[ArgCount] = ThisArg->Val->Pointer;
        
        else if (ThisArg->Typ->Base == TypeArray)
            ScanfArg[ArgCount] = &ThisArg->Val->ArrayMem[0];
        
        else
            ProgramFail(Parser, "non-pointer argument to IupGridBox() - argument %d after format", ArgCount+1);
    }
	for (; ArgCount < MAX_IUP_ARGS; ArgCount++)
	{
		ScanfArg[ArgCount] = NULL;
	}
    
    return IupGridBox((Ihandle*)ih, ScanfArg[0], ScanfArg[1], ScanfArg[2], ScanfArg[3], ScanfArg[4], ScanfArg[5], ScanfArg[6], ScanfArg[7], ScanfArg[8], ScanfArg[9],
		ScanfArg[10], ScanfArg[11], ScanfArg[12], ScanfArg[13], ScanfArg[14], ScanfArg[15]);
}


void StdiupGridBox(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = StdiupBaseGridBox(Parser, Param[0]->Val->Pointer, Param, NumArgs-1);
}

Ihandle *StdiupBaseMenu(struct ParseState *Parser, void * ih, struct Value **Param, int NumArgs)
{
    struct Value *ThisArg = Param[0];
    int ArgCount = 0;
    void *ScanfArg[MAX_IUP_ARGS];
    
    if (NumArgs > MAX_IUP_ARGS)
        ProgramFail(Parser, "too many arguments to IupGridBox() - %d max", MAX_IUP_ARGS);
    
    for (ArgCount = 0; ArgCount < NumArgs; ArgCount++)
    {
        ThisArg = (struct Value *)((char *)ThisArg + MEM_ALIGN(sizeof(struct Value) + TypeStackSizeValue(ThisArg)));
        
        if (ThisArg->Typ->Base == TypePointer) 
            ScanfArg[ArgCount] = ThisArg->Val->Pointer;
        
        else if (ThisArg->Typ->Base == TypeArray)
            ScanfArg[ArgCount] = &ThisArg->Val->ArrayMem[0];
        
        else
            ProgramFail(Parser, "non-pointer argument to IupGridBox() - argument %d after format", ArgCount+1);
    }
	for (; ArgCount < MAX_IUP_ARGS; ArgCount++)
	{
		ScanfArg[ArgCount] = NULL;
	}
    
    return IupMenu((Ihandle*)ih, ScanfArg[0], ScanfArg[1], ScanfArg[2], ScanfArg[3], ScanfArg[4], ScanfArg[5], ScanfArg[6], ScanfArg[7], ScanfArg[8], ScanfArg[9],
		ScanfArg[10], ScanfArg[11], ScanfArg[12], ScanfArg[13], ScanfArg[14], ScanfArg[15]);
}


void StdiupMenu(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
	ReturnValue->Val->Pointer = StdiupBaseMenu(Parser, Param[0]->Val->Pointer, Param, NumArgs-1);
}


void StdiupLabel(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = IupLabel(Param[0]->Val->Pointer);
}

void StdiupImage(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
	ReturnValue->Val->Pointer = IupImage(Param[0]->Val->Integer,Param[1]->Val->Integer,Param[2]->Val->Pointer);
}

void StdiupImageRGB(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = IupImageRGB(Param[0]->Val->Integer,Param[1]->Val->Integer,Param[2]->Val->Pointer);
}

void StdiupImageRGBA(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = IupImageRGBA(Param[0]->Val->Integer,Param[1]->Val->Integer,Param[2]->Val->Pointer);
}

void StdiupSetHandle(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = IupSetHandle(Param[0]->Val->Pointer,Param[1]->Val->Pointer);
}

void StdiupSetAttributeHandle(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    IupSetAttributeHandle(Param[0]->Val->Pointer,Param[1]->Val->Pointer, Param[2]->Val->Pointer);
}

void StdiupGetAttributeHandle(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = IupGetAttributeHandle(Param[0]->Val->Pointer,Param[1]->Val->Pointer);
}


void StdiupConfig(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Pointer = IupConfig();
}

void StdiupConfigLoad(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Integer = IupConfigLoad(Param[0]->Val->Pointer);
}

void StdiupConfigSave(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    ReturnValue->Val->Integer = IupConfigSave(Param[0]->Val->Pointer);
}

void StdiupConfigDialogShow(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    IupConfigDialogShow(Param[0]->Val->Pointer, Param[1]->Val->Pointer, Param[2]->Val->Pointer);
}

void StdiupConfigDialogClosed(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs)
{
    IupConfigDialogClosed(Param[0]->Val->Pointer, Param[1]->Val->Pointer, Param[2]->Val->Pointer);
}


void StdiupText(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupText(Param[0]->Val->Pointer);
}

void StdiupTimer(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupTimer();
}

void StdiupList(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupList(Param[0]->Val->Pointer);
}

void StdiupToggle(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupToggle(Param[0]->Val->Pointer, Param[1]->Val->Pointer);
}

void StdiupProgressBar(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupProgressBar();
}

void StdiupVal(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupVal(Param[0]->Val->Pointer);
}

void StdiupSpace(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupSpace();
}

void StdiupScrollBox(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupScrollBox(Param[0]->Val->Pointer);
}

void StdiupFrame(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupFrame(Param[0]->Val->Pointer);
}

void StdiupItem(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupItem(Param[0]->Val->Pointer,Param[1]->Val->Pointer );
}

void StdiupSubmenu(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupSubmenu(Param[0]->Val->Pointer,Param[1]->Val->Pointer );
}

void StdiupSeparator(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupSeparator();
}

void StdiupGetInt(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Integer = IupGetInt(Param[0]->Val->Pointer,Param[1]->Val->Pointer);
}

void StdiupFileDlg(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupFileDlg();
}

void StdiupMessageDlg(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupMessageDlg();
}

void StdiupColorDlg(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupColorDlg();
}

void StdiupFontDlg(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupFontDlg();
}

void StdiupProgressDlg(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupProgressDlg();
}

void StdiupCanvas(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupCanvas(Param[0]->Val->Pointer);
}

void StdiupTree(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupTree();
}

void StdiupDatePick(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupDatePick();
}

void StdiupCalendar(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupCalendar();
}

void StdiupColorbar(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupColorbar();
}

void StdiupGauge(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupGauge();
}

void StdiupDial(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupDial(Param[0]->Val->Pointer);
}

void StdiupColorBrowser(struct ParseState *Parser, struct Value *ReturnValue, struct Value **Param, int NumArgs) 
{
    ReturnValue->Val->Pointer = IupColorBrowser();
}



/* handy structure definitions */
const char StdiupDefs[] = "\
typedef void* Ihandle; \
";




/* all conio functions */
struct LibraryFunction StdiupFunctions[] =
{
    { StdiupOpen,   			"int IupOpen(int *argc, char ***argv);" },
    { StdiupClose,  			"void IupClose(void);" },
    { StdiupMainLoop,   		"int IupMainLoop(void);" },
    { StdiupMessage,   			"void IupMessage(char *title, char *msg);" },
    { StdiupGetDialog, 			"Ihandle* IupGetDialog(Ihandle* ih);" },
    { StdiupDestroy,   			"void IupDestroy(Ihandle* ih);" },
    { StdiupGetDialogChild,   	"Ihandle* IupGetDialogChild(Ihandle* ih, char* name);" },
    { StdiupShow,   			"int IupShow(Ihandle* ih);" },
    { StdiupShowXY,   			"int IupShowXY(Ihandle* ih, int x, int y);"},
    { StdiupHide,   			"int IupHide(Ihandle* ih);"},
    { StdiupPopup,   			"int IupPopup(Ihandle* ih, int x, int y);" },
    { StdiupSetAttribute,   	"void IupSetAttribute(Ihandle* ih, char* name, char* value);" },
    { StdiupGetAttribute,   	"char* IupGetAttribute(Ihandle* ih, char* name);" },
    { StdiupSetGlobal,   		"void IupSetGlobal(char* name, char* value);" },
    { StdiupGetGlobal,   		"char* IupGetGlobal(char* name);" },
    { StdiupGetCallback,   		"char* IupGetCallback(Ihandle* ih, char *name);" },
    { StdiupSetCallback,   		"void  IupSetCallback(Ihandle* ih, char *name, char* funcName);" },
    { StdiupDialog,   			"Ihandle* IupDialog(Ihandle* child);" },
    { StdiupButton,   			"Ihandle* IupButton(char* title, char* action);" },
    { StdiupVbox,   			"Ihandle* IupVbox(Ihandle* child, ...);" },
    { StdiupHbox,   			"Ihandle* IupHbox(Ihandle* child, ...);" },
    { StdiupGridBox,   			"Ihandle* IupGridBox(Ihandle* child, ...);" },
    { StdiupMenu,   			"Ihandle* IupMenu(Ihandle* child, ...);" },
    { StdiupLabel,   			"Ihandle* IupLabel(char* title);" },
    { StdiupImage,   			"Ihandle* IupImage(int width, int height, unsigned char *pixmap);" },
    { StdiupImageRGB,   		"Ihandle* IupImageRGB(int width, int height, unsigned char *pixmap);" },
    { StdiupImageRGBA,   		"Ihandle* IupImageRGBA(int width, int height, unsigned char *pixmap);" },
    { StdiupSetHandle,   		"Ihandle* IupSetHandle(char *name, Ihandle* ih);" },
    { StdiupSetAttributeHandle, "void IupSetAttributeHandle(Ihandle* ih, char* name, Ihandle* ih_named);" },
	{ StdiupGetAttributeHandle, "Ihandle* IupGetAttributeHandle(Ihandle* ih, char* name);"},
    { StdiupConfig,   			"Ihandle* IupConfig(void);" },
	{ StdiupConfigLoad,   		"int IupConfigLoad(Ihandle* ih);" },
	{ StdiupConfigSave,   		"int IupConfigSave(Ihandle* ih);" },
	{ StdiupConfigDialogShow,   "void IupConfigDialogShow(Ihandle* ih, Ihandle* dialog, char* name);"},
	{ StdiupConfigDialogClosed, "void IupConfigDialogClosed(Ihandle* ih, Ihandle* dialog, char* name);" },
	{ StdiupTimer, 				"Ihandle* IupTimer(void);" },
	{ StdiupText, 				"Ihandle* IupText(char* action);" },
	{ StdiupList, 				"Ihandle* IupList(char* action);" },
	{ StdiupToggle, 			"Ihandle* IupToggle(char* title, char* action);" },
	{ StdiupProgressBar, 		"Ihandle* IupProgressBar(void);" },
	{ StdiupVal, 				"Ihandle* IupVal(char *type);" },
	{ StdiupSpace, 				"Ihandle* IupSpace(void);" },
	{ StdiupScrollBox, 			"Ihandle* IupScrollBox(Ihandle* child);" },
	{ StdiupFrame, 				"Ihandle* IupFrame(Ihandle* child);" },
	{ StdiupItem, 				"Ihandle* IupItem(char* title, char* action);" },
	{ StdiupSubmenu, 			"Ihandle* IupSubmenu(char* title, Ihandle* child);"},
	{ StdiupSeparator, 			"Ihandle* IupSeparator(void);" },
	{ StdiupGetInt, 			"int IupGetInt(Ihandle* ih, char* name);" },
    { StdiupFileDlg, 			"Ihandle* IupFileDlg(void);" },
    { StdiupMessageDlg, 		"Ihandle* IupMessageDlg(void);" },
    { StdiupColorDlg, 			"Ihandle* IupColorDlg(void);" },
    { StdiupFontDlg, 			"Ihandle* IupFontDlg(void);" },
    { StdiupProgressDlg, 		"Ihandle* IupProgressDlg(void);" },
    { StdiupCanvas, 			"Ihandle* IupCanvas(char* action);" },
    { StdiupTree, 				"Ihandle* IupTree(void);" },
    { StdiupDatePick, 			"Ihandle* IupDatePick(void);" },
    { StdiupCalendar, 			"Ihandle* IupCalendar(void);" },
	{ StdiupColorbar, 			"Ihandle* IupColorbar(void);" },
	{ StdiupGauge, 				"Ihandle* IupGauge(void);" },
	{ StdiupDial, 				"Ihandle* IupDial(char* type);" },
	{ StdiupColorBrowser, 		"Ihandle* IupColorBrowser(void);" },
    
    
    { NULL,         NULL }
};

static int ZeroValue = 0;

/************************************************************************/
/*                   Callback Return Values                             */
/************************************************************************/
static int IUP_IGNOREValue 	= IUP_IGNORE;
static int IUP_DEFAULTValue = IUP_DEFAULT;
static int IUP_CLOSEValue	= IUP_CLOSE;
static int IUP_CONTINUEValue =IUP_CONTINUE;

/************************************************************************/
/*           IupPopup and IupShowXY Parameter Values                    */
/************************************************************************/
static int IUP_CENTERValue 	=        IUP_CENTER;  /* 65535 */
static int IUP_LEFTValue 	=        IUP_LEFT;  /* 65534 */
static int IUP_RIGHTValue 	=        IUP_RIGHT;  /* 65533 */
static int IUP_MOUSEPOSValue 	=    IUP_MOUSEPOS;  /* 65532 */
static int IUP_CURRENTValue 	=    IUP_CURRENT;  /* 65531 */
static int IUP_CENTERPARENTValue 	=IUP_CENTERPARENT;  /* 65530 */
static int IUP_TOPValue 	=        IUP_TOP;
static int IUP_BOTTOMValue 	=        IUP_BOTTOM;


/************************************************************************/
/*                   Common Flags and Return Values                     */
/************************************************************************/
static int IUP_ERRORValue = IUP_ERROR;
static int IUP_NOERRORValue = IUP_NOERROR;
static int IUP_OPENEDValue = IUP_OPENED;
static int IUP_INVALIDValue = IUP_INVALID;
static int IUP_INVALID_IDValue = IUP_INVALID_ID;



/* creates various system-dependent definitions */
void StdiupSetupFunc(void)
{   
	/* make a "struct Ihandle_" which is the same size as a native Ihandle_ structure */
    TypeCreateOpaqueStruct(NULL, TableStrRegister("Ihandle"), sizeof(void*));

    /* define NULL, TRUE and FALSE */
    if (!VariableDefined(TableStrRegister("NULL")))
    {
        VariableDefinePlatformVar(NULL, "NULL", &IntType, (union AnyValue *)&ZeroValue, FALSE);
    }

	VariableDefinePlatformVar(NULL, "IUP_IGNORE", &IntType, (union AnyValue *)&IUP_IGNOREValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_DEFAULT", &IntType, (union AnyValue *)&IUP_DEFAULTValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_CLOSE", &IntType, (union AnyValue *)&IUP_CLOSEValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_CONTINUE", &IntType, (union AnyValue *)&IUP_CONTINUEValue, FALSE);
	
	VariableDefinePlatformVar(NULL, "IUP_CENTER", &IntType, (union AnyValue *)&IUP_CENTERValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_LEFT", &IntType, (union AnyValue *)&IUP_LEFTValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_RIGHT", &IntType, (union AnyValue *)&IUP_RIGHTValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_MOUSEPOS", &IntType, (union AnyValue *)&IUP_MOUSEPOSValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_CURRENT", &IntType, (union AnyValue *)&IUP_CURRENTValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_CENTERPARENT", &IntType, (union AnyValue *)&IUP_CENTERPARENTValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_TOP", &IntType, (union AnyValue *)&IUP_TOPValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_BOTTOM", &IntType, (union AnyValue *)&IUP_BOTTOMValue, FALSE);

	VariableDefinePlatformVar(NULL, "IUP_ERROR", &IntType, (union AnyValue *)&IUP_ERRORValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_NOERROR", &IntType, (union AnyValue *)&IUP_NOERRORValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_OPENED", &IntType, (union AnyValue *)&IUP_OPENEDValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_INVALID", &IntType, (union AnyValue *)&IUP_INVALIDValue, FALSE);
	VariableDefinePlatformVar(NULL, "IUP_INVALID_ID", &IntType, (union AnyValue *)&IUP_INVALID_IDValue, FALSE);
}


#endif

